﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace QUFLEET.Wpf
{
    class ViewModel : ViewModelBase
    {
        private AirportLogic logic;
		private AirportVM selectedAirport;
		private ObservableCollection<AirportVM> allAirports;

		public ObservableCollection<AirportVM> AllAirports
		{
			get { return allAirports; }
			set { Set(ref allAirports, value); }
		}


		public AirportVM SelectedAirport
		{
			get { return selectedAirport; }
			set { Set(ref selectedAirport, value); }
		}

		public ICommand AddCommand { get; private set; }
		public ICommand DelCommand { get; private set; }
		public ICommand LoadCommand { get; private set; }

		public ViewModel()
		{
			logic = new AirportLogic();
			AddCommand = new RelayCommand(() => logic.ApiAddAirport(selectedAirport));
			DelCommand = new RelayCommand(() => logic.ApiDelAirport(selectedAirport));
			LoadCommand = new RelayCommand(() => AllAirports = new ObservableCollection<AirportVM>(logic.ApiGetAirports()));
		}

	}
}
