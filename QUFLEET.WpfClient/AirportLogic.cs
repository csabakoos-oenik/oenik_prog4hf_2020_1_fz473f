﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace QUFLEET.Wpf
{
    class AirportLogic
    {
        string url = "http://localhost:57853/api/AirportsApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "COMPLETED" : "FAILED";
            Messenger.Default.Send(msg, "AirportResult");
        }

        public List<AirportVM> ApiGetAirports()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<AirportVM>>(json);
            SendMessage(true);
            return list;
        }

        public void ApiDelAirport(AirportVM airport)
        {
            bool success = false;
            if (airport != null)
            {
                string json = client.GetStringAsync(url + "del/" + airport.ID).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        public bool ApiAddAirport(AirportVM airport)
        {
            string json = client.GetStringAsync(url + "add").Result;

            Dictionary<string, string> postData = new Dictionary<string, string>();
            postData.Add(nameof(AirportVM.IATA), airport.IATA);
            postData.Add(nameof(AirportVM.ICAO), airport.ICAO);
            postData.Add(nameof(AirportVM.FAA), airport.FAA);
            postData.Add(nameof(AirportVM.Country), airport.Country);
            postData.Add(nameof(AirportVM.City), airport.City);

            json = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }
    }
}
