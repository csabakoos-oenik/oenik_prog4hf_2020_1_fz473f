﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QUFLEET.Wpf
{
    class AirportVM : ObservableObject
    {
		private int id;

		public int ID
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		private string iata;

		public string IATA
		{
			get { return iata; }
			set { Set(ref iata, value); }
		}

		private string icao;

		public string ICAO
		{
			get { return icao; }
			set { Set(ref icao, value); }
		}

		private string faa;

		public string FAA
		{
			get { return faa; }
			set { Set(ref faa, value); }
		}

		private string country;

		public string Country
		{
			get { return country; }
			set { Set(ref country, value); }
		}

		private string city;

		public string City
		{
			get { return city; }
			set { Set(ref city, value); }
		}

		public void Copy(AirportVM airport)
		{
			if (airport == null)
			{
				return;
			}
			else
			{
				this.ID = airport.ID;
				this.IATA = airport.IATA;
				this.ICAO = airport.ICAO;
				this.FAA = airport.FAA;
				this.Country = airport.Country;
				this.City = airport.City;
			}
		}
	}
}
