﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QUFLEET.Web.Models
{
    public class AirportsViewModel
    {
        public AirportsModel EditedAirport { get; set; }
        public List<AirportsModel> AirportsList { get; set; }
    }
}