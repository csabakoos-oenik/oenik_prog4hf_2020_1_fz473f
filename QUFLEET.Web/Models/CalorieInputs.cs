﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalorieCalculator.Models
{
    public class CalorieInputs
    {
        public string Name { get; set; }
        public double Weight { get; set; }
        public List<Exercise> Excercises { get; private set; }

        public string Exercise { get; set; }

        public int Duration { get; set; }

        public CalorieInputs()
        {
            Excercises = new List<Exercise>();
            Excercises.Add(new Exercise("Running", 1000));
            Excercises.Add(new Exercise("Yoga", 400));
            Excercises.Add(new Exercise("Pilates", 472));
            Excercises.Add(new Exercise("Hiking", 700));
            Excercises.Add(new Exercise("Swimming", 1000));
            Excercises.Add(new Exercise("Bicycle", 600));
        }
    }
}