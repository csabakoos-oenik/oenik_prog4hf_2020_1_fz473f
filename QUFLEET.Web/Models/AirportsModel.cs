﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QUFLEET.Web.Models
{
    public class AirportsModel
    {
        [Display(Name = "ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "IATA")]
        [StringLength(3, MinimumLength = 3)]
        public string IATA { get; set; }

        [Display(Name = "ICAO")]
        [StringLength(4, MinimumLength = 4)]
        public string ICAO { get; set; }

        [Display(Name = "FAA")]
        [StringLength(3, MinimumLength = 3)]
        public string FAA { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "City")]
        [Required]
        public string City { get; set; }
    }
}