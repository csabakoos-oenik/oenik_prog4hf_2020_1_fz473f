﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QUFLEET.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<QUFLEET.Data.AIRPORT, QUFLEET.Web.Models.AirportsModel>()
                .ForMember(dest => dest.ID, map => map.MapFrom(src => src.AIRPORT_ID))
                .ForMember(dest => dest.IATA, map => map.MapFrom(src => src.IATA))
                .ForMember(dest => dest.ICAO, map => map.MapFrom(src => src.ICAO))
                .ForMember(dest => dest.FAA, map => map.MapFrom(src => src.FAA))
                .ForMember(dest => dest.Country, map => map.MapFrom(src => src.COUNTRY))
                .ForMember(dest => dest.City, map => map.MapFrom(src => src.CITY));
            });

            return config.CreateMapper();
        }
    }
}