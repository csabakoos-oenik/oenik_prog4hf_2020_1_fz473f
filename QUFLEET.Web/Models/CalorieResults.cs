﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalorieCalculator.Models
{
    public class CalorieResults
    {
        public string Name { get; set; }
        public double Weight { get; set; }
        public int Duration { get; set; }
        public double BurnedCalories { get; set; }
    }
}