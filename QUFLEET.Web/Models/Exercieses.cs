﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalorieCalculator.Models
{
    public class Exercise
    {
        public string Name { get; set; }
        public int BurnedCalories { get; set; }

        public Exercise(string name, int burnedCalories)
        {
            this.Name = name;
            this.BurnedCalories = burnedCalories;
        }
    }
}