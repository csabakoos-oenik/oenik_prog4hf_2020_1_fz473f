﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CalorieCalculator.Models;

namespace CalorieCalculator.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult Results(CalorieInputs calorieInputs)
        {
            CalorieResults calorieResults = new CalorieResults();

            calorieResults.Name = calorieInputs.Name;
            calorieResults.Weight = calorieInputs.Weight;
            calorieResults.Duration = calorieInputs.Duration;
            calorieResults.BurnedCalories = (calorieInputs.Weight / 100) * (calorieInputs.Duration / 60) * calorieInputs.Excercises.First(x => x.Name == calorieInputs.Exercise).BurnedCalories;

            return View("Results", calorieResults);
        }
    }
}