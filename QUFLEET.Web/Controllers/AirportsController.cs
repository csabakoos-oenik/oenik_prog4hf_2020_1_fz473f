﻿using AutoMapper;
using QUFLEET.Logic;
using QUFLEET.Data;
using QUFLEET.Repository;
using QUFLEET.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QUFLEET.Web.Controllers
{
    public class AirportsController : Controller
    {
        IAirportsLogic logic;
        IMapper mapper;
        AirportsViewModel vm;

        public AirportsController()
        {
            AirportsRepository repository = new AirportsRepository();
            logic = new AirportsLogic();
            mapper = MapperFactory.CreateMapper();
            vm = new AirportsViewModel();
            vm.EditedAirport = new AirportsModel();
            IQueryable<AIRPORT> airports = logic.GetAll();
            vm.AirportsList = mapper.Map<IQueryable<AIRPORT>, List<Models.AirportsModel>>(airports);

        }

        private AirportsModel GetAirportsModel(int id)
        {
            AIRPORT airport = logic.GetOne(id);
            return mapper.Map<Data.AIRPORT, Models.AirportsModel>(airport);
        }

        // GET: Airports
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("AirportIndex", vm);
        }

        // GET: Airports/Details/5
        public ActionResult Details(int id)
        {
            return View("AirportsDetails", GetAirportsModel(id));
        }

        public ActionResult Delete(int id)
        {
            TempData["editResult"] = "Delete OK";
            logic.DeleteAirport(id);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedAirport = GetAirportsModel(id);
            return View("AirportIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(AirportsModel model, string editAction)
        {
            if (ModelState.IsValid && model != null)
            {
                TempData["editResult"] = "Edit OK";

                if (editAction == "AddNew")
                {
                    logic.CreateAirport(model.IATA, model.ICAO, model.FAA, model.Country, model.City);
                }
                else
                {
                    logic.UpdateFAA(model.ID, model.FAA);
                    logic.UpdateIATA(model.ID, model.IATA);
                    logic.UpdateICAO(model.ID, model.ICAO);
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedAirport = model;
                return View("AirportIndex", vm);
            }
        }
    }
}
