﻿using AutoMapper;
using QUFLEET.Logic;
using QUFLEET.Data;
using QUFLEET.Repository;
using QUFLEET.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace QUFLEET.Web.Controllers
{
    public class AirportsApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        IAirportsLogic logic;
        IMapper mapper;

        public AirportsApiController()
        {
            AirportsRepository repository = new AirportsRepository();
            logic = new AirportsLogic();
            mapper = MapperFactory.CreateMapper();
        }

        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.AirportsModel> GetAll()
        {
            var airports = logic.GetAll();
            return mapper.Map<IQueryable<AIRPORT>, List<Models.AirportsModel>>(airports);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DeleteAirport(int id)
        {
            logic.DeleteAirport(id);

            bool success = logic.GetOne(id) == null;

            return new ApiResult() { OperationResult = success };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult CreateAirport(AirportsModel airport)
        {
            logic.CreateAirport(airport.IATA, airport.ICAO, airport.FAA, airport.Country, airport.City);
            return new ApiResult() { OperationResult = true };
        }

        [ActionName("modfaa")]
        [HttpPost]
        public ApiResult UpdateFAA(int id, string faa)
        {
            logic.UpdateFAA(id, faa);
            bool success = logic.GetOne(id).FAA == faa;

            return new ApiResult() { OperationResult = success };
        }

        [ActionName("modicao")]
        [HttpPost]
        public ApiResult UpdateICAO(int id, string icao)
        {
            logic.UpdateICAO(id, icao);
            bool success = logic.GetOne(id).ICAO == icao;

            return new ApiResult() { OperationResult = success };
        }

        [ActionName("modiata")]
        [HttpPost]
        public ApiResult UpdateIATA(int id, string iata)
        {
            logic.UpdateIATA(id, iata);
            bool success = logic.GetOne(id).IATA == iata;

            return new ApiResult() { OperationResult = success };
        }
    }
}
