﻿// <copyright file="NotExistingOptionException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Program
{
    using System;

    /// <summary>
    /// This exception helps determine whether if the submitted option by the user if a valid one.
    /// </summary>
    public class NotExistingOptionException : FormatException
    {
        /// <inheritdoc/>
        public override string Message => "Selected option is not exsisting! Please select a new one!";
    }
}
