var hierarchy =
[
    [ "QUFLEET.Data.AIRPORT", "class_q_u_f_l_e_e_t_1_1_data_1_1_a_i_r_p_o_r_t.html", null ],
    [ "QUFLEET.Logic.Tests.AirportsLogicTests", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_airports_logic_tests.html", null ],
    [ "QUFLEET.Data.BOOKING", "class_q_u_f_l_e_e_t_1_1_data_1_1_b_o_o_k_i_n_g.html", null ],
    [ "DbContext", null, [
      [ "QUFLEET.Data::QUJETDatabaseEntities", "class_q_u_f_l_e_e_t_1_1_data_1_1_q_u_j_e_t_database_entities.html", null ]
    ] ],
    [ "QUFLEET.Data.EMPLOYEE", "class_q_u_f_l_e_e_t_1_1_data_1_1_e_m_p_l_o_y_e_e.html", null ],
    [ "QUFLEET.Logic.Tests.EmployeesLogicTests", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_employees_logic_tests.html", null ],
    [ "QUFLEET.Data.FLEET", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_e_e_t.html", null ],
    [ "QUFLEET.Logic.Tests.FleetLogicTests", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_fleet_logic_tests.html", null ],
    [ "QUFLEET.Data.FLIGHT", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t.html", null ],
    [ "QUFLEET.Data.FLIGHT_ATTENDANTS", "class_q_u_f_l_e_e_t_1_1_data_1_1_f_l_i_g_h_t___a_t_t_e_n_d_a_n_t_s.html", null ],
    [ "QUFLEET.Logic.Tests.FlightsLogicTests", "class_q_u_f_l_e_e_t_1_1_logic_1_1_tests_1_1_flights_logic_tests.html", null ],
    [ "FormatException", null, [
      [ "QUFLEET.Program.NotExistingOptionException", "class_q_u_f_l_e_e_t_1_1_program_1_1_not_existing_option_exception.html", null ]
    ] ],
    [ "QUFLEET.Logic.ILogic< T, T_ID >", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_logic.html", null ],
    [ "QUFLEET.Logic.ILogic< AIRPORT, int >", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_logic.html", [
      [ "QUFLEET.Logic.IAirportsLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_airports_logic.html", [
        [ "QUFLEET.Logic.AirportsLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_airports_logic.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Logic.ILogic< BOOKING, int >", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_logic.html", [
      [ "QUFLEET.Logic.IBookingsLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_bookings_logic.html", [
        [ "QUFLEET.Logic.BookingsLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_bookings_logic.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Logic.ILogic< EMPLOYEE, int >", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_logic.html", [
      [ "QUFLEET.Logic.IEmployeesLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_employees_logic.html", [
        [ "QUFLEET.Logic.EmployeesLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_employees_logic.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Logic.ILogic< FLEET, string >", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_logic.html", [
      [ "QUFLEET.Logic.IFleetLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_fleet_logic.html", [
        [ "QUFLEET.Logic.FleetLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_fleet_logic.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Logic.ILogic< FLIGHT, int >", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_logic.html", [
      [ "QUFLEET.Logic.IFlightsLogic", "interface_q_u_f_l_e_e_t_1_1_logic_1_1_i_flights_logic.html", [
        [ "QUFLEET.Logic.FlightsLogic", "class_q_u_f_l_e_e_t_1_1_logic_1_1_flights_logic.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Repository.IRepository< T, T_ID >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", null ],
    [ "QUFLEET.Repository.IRepository< AIRPORT, int >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", [
      [ "QUFLEET.Repository.IAirportsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_airports_repository.html", [
        [ "QUFLEET.Repository.AirportsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_airports_repository.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Repository.IRepository< BOOKING, int >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", [
      [ "QUFLEET.Repository.IBookingsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_bookings_repository.html", [
        [ "QUFLEET.Repository.BookingsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_bookings_repository.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Repository.IRepository< EMPLOYEE, int >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", [
      [ "QUFLEET.Repository.IEmployeesRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_employees_repository.html", [
        [ "QUFLEET.Repository.EmployeesRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_employees_repository.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Repository.IRepository< FLEET, string >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", [
      [ "QUFLEET.Repository.IFleetRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_fleet_repository.html", [
        [ "QUFLEET.Repository.FleetRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_fleet_repository.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Repository.IRepository< FLIGHT, int >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", [
      [ "QUFLEET.Repository.IFlightsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_flights_repository.html", [
        [ "QUFLEET.Repository.FlightsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flights_repository.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Repository.IRepository< FLIGHT_ATTENDANTS, int >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", [
      [ "QUFLEET.Repository.IFlightAttendantsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_flight_attendants_repository.html", [
        [ "QUFLEET.Repository.FlightAttendantsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_flight_attendants_repository.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Repository.IRepository< MAINTENANCE_CREW, int >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", [
      [ "QUFLEET.Repository.IMaintenanceCrewRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_maintenance_crew_repository.html", [
        [ "QUFLEET.Repository.MaintenanceCrewRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_maintenance_crew_repository.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Repository.IRepository< PILOT, int >", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_repository.html", [
      [ "QUFLEET.Repository.IPilotsRepository", "interface_q_u_f_l_e_e_t_1_1_repository_1_1_i_pilots_repository.html", [
        [ "QUFLEET.Repository.PilotsRepository", "class_q_u_f_l_e_e_t_1_1_repository_1_1_pilots_repository.html", null ]
      ] ]
    ] ],
    [ "QUFLEET.Data.MAINTENANCE_CREW", "class_q_u_f_l_e_e_t_1_1_data_1_1_m_a_i_n_t_e_n_a_n_c_e___c_r_e_w.html", null ],
    [ "QUFLEET.Data.PILOT", "class_q_u_f_l_e_e_t_1_1_data_1_1_p_i_l_o_t.html", null ]
];