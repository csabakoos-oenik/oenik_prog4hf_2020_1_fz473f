﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace pong
{
    class PongRenderer
    {
        PongModel model;

        public PongRenderer(PongModel model)
        {
            this.model = model;
        }

        public void DrawThings(DrawingContext context)
        {
            DrawingGroup drawingGroup = new DrawingGroup();

            GeometryDrawing background = new GeometryDrawing(Config.BackgroundBrush, new Pen(Config.BorderBrush, Config.Border), new RectangleGeometry(new Rect(0, 0, Config.Width, Config.Height)));
            GeometryDrawing ball = new GeometryDrawing(Config.BallBackgroundBrush, new Pen(Config.BallLineBrush, 1), new EllipseGeometry(model.Ball.Area));
            GeometryDrawing pad = new GeometryDrawing(Config.PadBackgroundBrush, new Pen(Config.PadLineBrush, 1), new RectangleGeometry(model.Pad.Area));

            FormattedText formattedText = new FormattedText(model.Errors.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Arial"), 16, Brushes.Black);
            GeometryDrawing text = new GeometryDrawing(null, new Pen(Brushes.Red, 3), formattedText.BuildGeometry(new Point(5, 5)));

            drawingGroup.Children.Add(background);
            drawingGroup.Children.Add(text);

            foreach (Star star in model.Stars)
            {
                GeometryDrawing newStar = new GeometryDrawing(Config.BallBackgroundBrush, new Pen(Config.BallLineBrush, 1), star.GetGeometry());
                drawingGroup.Children.Add(newStar);
            }

            foreach (Enemy enemy in model.Enemies)
            {
                GeometryDrawing newEnemy = new GeometryDrawing(Config.EnemyBackgroundBrush, new Pen(Config.EnemyLineBrush, 1), new RectangleGeometry(enemy.Area));
                drawingGroup.Children.Add(newEnemy);
            }

            drawingGroup.Children.Add(pad);
            drawingGroup.Children.Add(ball);

            context.DrawDrawing(drawingGroup);
        }
    }
}
