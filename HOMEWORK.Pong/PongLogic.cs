﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{
    class PongLogic
    {
        PongModel model;

        public enum Directions { Left, Right }

        public event EventHandler RefreshScreen;

        public PongLogic(PongModel model)
        {
            this.model = model;
        }

        public void MovePad(Directions direction)
        {
            if (direction == Directions.Left)
            {
                model.Pad.ChangeX(-10);
            }
            else
            {
                model.Pad.ChangeX(10);
            }

            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void JumpPad(double x)
        {
            model.Pad.SetXY(x, model.Pad.Area.Y);
        }

        private bool MoveShape(MyShape shape)
        {
            bool isFaulted = false;

            shape.ChangeX(shape.Dx);
            shape.ChangeY(shape.Dy);

            if (shape.Area.Left < 0 || shape.Area.Right > Config.Width)
            {
                shape.Dx = -shape.Dx;
            }

            if (shape.Area.Top < 0 || shape.Area.IntersectsWith(model.Pad.Area))
            {
                shape.Dy = -shape.Dy;
            }

            if (shape.Area.Bottom > Config.Height)
            {
                isFaulted = true;
                shape.SetXY(Config.Width / 2, Config.Height / 2);
            }

            return isFaulted;
        }

        public void MoveBall()
        {
            if (MoveShape(model.Ball))
            {
                model.Errors++;
            }

            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        
        public void AddStar()
        {
            model.Stars.Add(new Star(Config.Width / 2, Config.Height / 2, Config.BallSize / 2, 6));
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void MoveStar()
        {
            foreach (Star star in model.Stars)
            {
                if (MoveShape(star))
                {
                    model.Errors++;
                }

                RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        public void EnemyMoveShape(List<Enemy> enemies)
        {
            Random rnd = new Random();

            foreach (Enemy enemy in enemies.ToList())
            {
                if (rnd.Next(25) == rnd.Next(25))
                {
                    enemy.Dx = rnd.Next(-5, 5);
                    enemy.Dy = rnd.Next(-5, 5);
                }

                enemy.ChangeX(enemy.Dx);
                enemy.ChangeY(enemy.Dy);

                if (enemy.Area.Left <= 0 || enemy.Area.Right >= Config.Width)
                {
                    enemy.Dx = -enemy.Dx;
                }

                if (enemy.Area.Top <= 0 || enemy.Area.Bottom >= Config.Height)
                {
                    enemy.Dy = -enemy.Dy;
                }

                if (enemy.Area.IntersectsWith(model.Ball.Area))
                {
                    rnd = new Random();
                    enemies.Remove(enemy);
                    enemies.Add(new Enemy(rnd.Next(Config.BallSize, (int)Config.Width - Config.BallSize), rnd.Next(Config.BallSize, (int)Config.Height / 5)));
                    // enemy.SetXY(rnd.Next(Config.BallSize, (int)Config.Width - Config.BallSize), rnd.Next(Config.BallSize, (int)Config.Height / 5));

                    switch (rnd.Next(1,4))
                    {
                        case 1:
                            model.Ball.Dx = -model.Ball.Dx;
                            break;
                        case 2:
                            model.Ball.Dy = -model.Ball.Dy;
                            break;
                        case 3:
                            model.Ball.Dx = -model.Ball.Dx;
                            model.Ball.Dy = -model.Ball.Dy;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void MoveEnemy()
        {
            EnemyMoveShape(model.Enemies);
        }
    }
}
