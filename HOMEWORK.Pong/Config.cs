﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace pong
{
    public static class Config
    {
        public static Brush BorderBrush = Brushes.DarkGray;
        public static Brush BackgroundBrush = Brushes.Cyan;

        public static Brush BallLineBrush = Brushes.Red;
        public static Brush BallBackgroundBrush = Brushes.Yellow;

        public static Brush EnemyLineBrush = Brushes.CadetBlue;
        public static Brush EnemyBackgroundBrush = Brushes.Blue;

        public static Brush PadLineBrush = Brushes.Black;
        public static Brush PadBackgroundBrush = Brushes.Brown;

        public static double Width = 700;
        public static double Height = 300;
        public static int Border = 4;

        public static int BallSize = 20;
        public static int PadWidth = 100;
        public static int PadHeight = 20;
    }
}
