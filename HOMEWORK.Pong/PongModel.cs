﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{
    class PongModel
    {
        public int Errors { get; set; }
        public MyShape Pad { get; set; }
        public MyShape Ball { get; set; }

        public List<Star> Stars { get; set; }
        public List<Enemy> Enemies { get; set; }

        public PongModel()
        {
            Ball = new MyShape(Config.Width / 2 - Config.BallSize / 2, Config.Height / 2 - Config.BallSize / 2, Config.BallSize, Config.BallSize);
            Pad = new MyShape(Config.Width / 2 - Config.PadWidth / 2, Config.Height - Config.PadHeight, Config.PadWidth, Config.PadHeight);
            Stars = new List<Star>();
            Enemies = new List<Enemy>();

            Random rnd = new Random();

            Enemies.Add(new Enemy(rnd.Next(Config.BallSize, (int)Config.Width - Config.BallSize), rnd.Next(Config.BallSize, (int)Config.Height / 5)));
            Enemies.Add(new Enemy(rnd.Next(Config.BallSize, (int)Config.Width - Config.BallSize), rnd.Next(Config.BallSize, (int)Config.Height / 5)));
            Enemies.Add(new Enemy(rnd.Next(Config.BallSize, (int)Config.Width - Config.BallSize), rnd.Next(Config.BallSize, (int)Config.Height / 5)));
        }
    }
}
