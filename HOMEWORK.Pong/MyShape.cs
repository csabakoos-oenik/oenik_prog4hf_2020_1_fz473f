﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace pong
{
    class MyShape
    {
		private Rect area;

		public Rect Area
		{
			get { return area; }
		}

		public int Dx { get; set; }

		public int Dy { get; set; }

		public MyShape(double x, double y, double width, double height)
		{
			area = new Rect(x, y, width, height);
			Dx = 5;
			Dy = 5;
		}

		public void ChangeX(int diff)
		{
			area.X += diff;
		}

		public void ChangeY(int diff)
		{
			area.Y += diff;
		}

		public void SetXY(double x, double y)
		{
			area.X = x;
			area.Y = y;
		}
	}
}
