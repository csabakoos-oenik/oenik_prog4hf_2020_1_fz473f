﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace pong
{
    class PongControl : FrameworkElement
    {
        PongModel model;
        PongLogic logic;
        PongRenderer renderer;
        DispatcherTimer timer; 

        public PongControl()
        {
            Loaded += PongControl_Loaded;
        }

        private void PongControl_Loaded(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);

            model = new PongModel();
            logic = new PongLogic(model);
            renderer = new PongRenderer(model);

            if (win != null)
            {
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(40);
                timer.Tick += Timer_Tick;
                timer.Start();

                win.KeyDown += Win_KeyDown;
                MouseLeftButtonDown += PongControl_MouseLeftButtonDown;
            }

            logic.RefreshScreen += (obj, args) => InvalidateVisual();
            InvalidateVisual();
        }

        private void PongControl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            logic.JumpPad(e.GetPosition(this).X);
        }

        private void Win_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case System.Windows.Input.Key.Left:
                    logic.MovePad(PongLogic.Directions.Left); break;
                case System.Windows.Input.Key.Right:
                    logic.MovePad(PongLogic.Directions.Right); break;
                case System.Windows.Input.Key.Space:
                    logic.AddStar(); break;
                default:
                    break;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            logic.MoveBall();
            logic.MoveStar();
            logic.MoveEnemy();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (renderer != null)
            {
                renderer.DrawThings(drawingContext);
            }
        }
    }
}
