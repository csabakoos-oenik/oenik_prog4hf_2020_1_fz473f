﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{
    class Enemy : MyShape
    {
        public Enemy(double x, double y) : base(x, y, Config.BallSize, Config.BallSize)
        {
            Random rnd = new Random();
            Dx = rnd.Next(-5, 5);
            Dy = rnd.Next(-5, 5);
        }
    }
}
