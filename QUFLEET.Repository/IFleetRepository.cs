﻿// <copyright file="IFleetRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace QUFLEET.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using QUFLEET.Data;

    /// <summary>
    /// The FLEET's CRUD operations.
    /// </summary>
    public interface IFleetRepository : IRepository<FLEET, string>
    {
        // CRUD - create, read, update, delete

        // create

        /// <summary>
        /// Creates a new plane and adds it to the FLEET table.
        /// </summary>
        /// <param name="plane">The new plane.</param>
        void CreatePlane(FLEET plane);

        // update

        /// <summary>
        /// Updates the selected plane's callsign.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        /// <param name="callsign">The plane's new callsign.</param>
        void UpdateCallsign(string id, string callsign);

        /// <summary>
        /// Updates the selected plane's capacity.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        /// <param name="capacity">The plane's new capacity.</param>
        void UpdateCapacity(string id, short capacity);

        /// <summary>
        /// Updates the selected plane's status.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        /// <param name="status">The plane's new status.</param>
        void UpdateStatus(string id, string status);

        // delete

        /// <summary>
        /// Deletes the selected plane from the FLEET table.
        /// </summary>
        /// <param name="id">The plane's id.</param>
        void DeletePlane(string id);
    }
}
