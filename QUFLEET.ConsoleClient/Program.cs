﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace QUFLEET.ConsoleClient
{
    public class AirportsModel
    {
        public int ID { get; set; }

        public string IATA { get; set; }

        public string ICAO { get; set; }

        public string FAA { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public override string ToString()
        {
            return $"ID={ID} IATA={IATA} ICAO={ICAO} FAA={FAA} COUNTRY={Country} CITY={City}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:57853/api/AirportsApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<AirportsModel>>(json);

                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(AirportsModel.IATA), "");
                postData.Add(nameof(AirportsModel.ICAO), "");
                postData.Add(nameof(AirportsModel.FAA), "");
                postData.Add(nameof(AirportsModel.Country), "Country");
                postData.Add(nameof(AirportsModel.City), "City");



                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ADD: " + json);
                Console.ReadLine();



                int airportID = JsonConvert.DeserializeObject<List<AirportsModel>>(json).First(x => x.City == "City").ID;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(AirportsModel.ID), airportID.ToString());
                postData.Add(nameof(AirportsModel.FAA), "FAA");

                response = client.PostAsync(url + "modfaa", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ADD: " + json);
                Console.ReadLine();



                response = client.GetStringAsync(url + "del/" + airportID).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ADD: " + json);
                Console.ReadLine();
            }

            Console.ReadLine();
        }
    }
}
